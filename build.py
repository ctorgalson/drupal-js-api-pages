#!/usr/bin/env python3

import json
import os
import re
import shutil
import subprocess


tmp_dir = './tmp'
src_dir = './src'
dist_dir = './public'
drupal_repo = 'https://git.drupalcode.org/project/drupal.git'
drupal_dest = 'drupal'
jsdoc_config_src = '{}/jsdoc.json'.format(src_dir)
jsdoc_config_dest = '{}/jsdoc.json'.format(tmp_dir)


def clone_drupal_codebase():
    """Clones Drupal codebase to build directory."""
    subprocess.run(['git', 'clone', drupal_repo, "{}/{}".format(tmp_dir,
                   drupal_dest)], check=True)


def create_jsdoc_menu_items(branches):
    """Reads source jsdoc.json, adds a menu-item per-branch (plus an extra one
    for the current branch), and writes a new jsdoc.json to the temp directory
    for use by jsdoc in the build process."""
    menu = {
        'Current ({})'.format(branches['current']): {
            'href': '/',
            'class': 'menu-item',
            'id': 'branch-current',
        }
    }

    for branch in branches['all']:
        if branch == branches['current']:
            href = '/'
        else:
            href = '/{}'.format(branch)

        menu[branch] = {
            'href': href,
            'class': 'menu-item',
            'id': 'branch-{}'.format(branch.replace('.', ''))
        }

    return menu


def create_jsdocs(drupal_branches):
    """Creates jsdoc documents in custom-named directory for each Drupal 9.x
    and 10.x branch. Also creates jsdoc documents for current branch in root of
    public directory."""
    bin_dir = get_npm_bin()
    modules_dir = bin_dir.replace('/.bin', '')
    jsdoc = '{}/jsdoc'.format(bin_dir)
    template = '{}/docdash'.format(modules_dir)
    configure = jsdoc_config_dest

    remove_public_files()

    for branch in drupal_branches['all']:
        drupal_dir = '{}/{}'.format(tmp_dir, drupal_dest)
        drupal_js_dir = '{}/core/misc'.format(drupal_dir)

        if branch == drupal_branches['current']:
            destination = dist_dir
        else:
            destination = '{}/{}'.format(dist_dir, branch)

        subprocess.run(['git', '-C', drupal_dir, 'checkout', branch],
                       check=True)

        """Not using `check=True` in this subprocess.run() because the current
        state of Drupal's js causes jsdoc to return a nonzero rc even when the
        command succeeds."""
        subprocess.run([jsdoc, drupal_js_dir, '--template', template,
                       '--destination', destination, '--configure', configure])


def create_tmp_directory():
    """Creates tmp directory at beginning of build task."""
    os.mkdir(tmp_dir)


def get_drupal_branches():
    """Returns an object containing a string for the current branch, and a
    list of all the available 9.x and 10.x branches."""
    output = subprocess.run(['git', '-C', "{}/{}".format(tmp_dir, drupal_dest),
                            'branch', '-a'], check=True, capture_output=True,
                            text=True)

    output_lines = list(reversed(output.stdout.splitlines()))

    branches = {
        'all': [],
        'current': output_lines[0].replace('  remotes/origin/HEAD -> origin/',
                                           '')
    }

    for branch in output_lines:
        match = re.findall('^\\s+remotes\\/origin\\/([9|10].*)$', branch)

        if match:
            branches['all'].append(match[0])

    return branches


def get_npm_bin():
    """Returns path to project's ./node_modules/.bin/ directory."""
    return subprocess.run(['npm', 'bin'], check=True, capture_output=True,
                          text=True).stdout.strip()


def remove_public_files():
    """Removes contents of public directory immediately prior to jsdoc
    generation."""
    for filename in os.listdir(dist_dir):
        filepath = os.path.join(dist_dir, filename)

        if filename != '.gitkeep':
            try:
                shutil.rmtree(filepath)
            except OSError:
                os.remove(filepath)


def remove_tmp_files():
    """Removes tmp directory when jsdoc generation completes."""
    shutil.rmtree(tmp_dir)


def main():
    """Coordinates build tasks."""
    create_tmp_directory()

    clone_drupal_codebase()

    drupal_branches = get_drupal_branches()

    with open(jsdoc_config_src) as config_src:
        config = json.load(config_src)

    config['docdash']['menu'] = {
        **config['docdash']['menu'],
        **create_jsdoc_menu_items(drupal_branches)
    }

    with open(jsdoc_config_dest, 'w') as config_dest:
        json.dump(config, config_dest)

    create_jsdocs(drupal_branches)

    remove_tmp_files()


main()
